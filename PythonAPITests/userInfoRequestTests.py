import unittest
from userInfoRequests import *

class TestUserInfo(unittest.TestCase):

    def test_register_new_user(self):
        status = register_new_user()
        self.assertEqual(status, 'SUCCESS', "new user is registered")

    def test_review_users(self):
        status = review_users()
        self.assertEqual(status, 'SUCCESS', "users review is successful")

    def test_update_personal_info(self):
        status = update_personal_info()
        self.assertEqual(status, 'SUCCESS', "Personal information is updated")

    def test_get_personal_info(self):
        status = get_personal_info()
        self.assertEqual(status, 'SUCCESS', "Personal information are accessed")

