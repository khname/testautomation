import  json
import  requests

personalDetails = {'username': 'duqq', 'password': 'eruwdd', 'firstname': 'don', 'lastname': 'don',
                'phone': '0445566'}
personalInfo = {'firstname': 'rami', 'lastname': 'don', 'phone': '0554466'}

username = personalDetails['username']
password = personalDetails['password']

host = "http://0.0.0.0:8080/api/users"


def return_token():

    """this function return the token for user"""

    url = "http://0.0.0.0:8080/api/auth/token"
    headers = {'Content-type': 'application/json'}
    response = requests.get(url, auth=(username, password), headers=headers)
    data = json.loads(response.content)
    return data['token']

def register_new_user():

    """this function register the new users"""

    headers = {'content-type': 'application/json'}
    response = requests.post(host, json=personalDetails, headers=headers)
    data = json.loads(response.content)
    return data['status']

def review_users():

    """this function review the users"""

    headers = {'content-type': 'application/json'}
    response = requests.get(host, headers=headers)
    data = json.loads(response.content)
    return data['status']


def get_personal_info():

    """this function retrives the personal information"""

    url = (host + '/' + username)
    token = return_token()
    headers = {'Content-type': 'application/json', 'Token': token}
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    return data['status']

def update_personal_info():

    """this function updates the user information"""

    url = (host + '/' + username)
    token = return_token()
    headers = {'content-type': 'application/json', 'token': token}
    response = requests.put(url, json=personalInfo, headers=headers)
    data = json.loads(response.content)
    return data['status']


