*** Settings ***
Documentation     Test suite verifies that user can register through web portal
...               Users can review information from the main view

Library          SeleniumLibrary
Force Tags       UI Tests
Suite Setup      Suite setup
Suite Teardown   Suite TearDown

*** Variables ***
${user_name}      user
${password}       password
${first_name}     John
${last_name}      surname
${phone_number}   044556677

${URL}            http://10.0.2.15:8080/
${browser}        firefox

*** Test Cases ***
Register Credentials
  [Documentation]   This test case verifies that the user is able register information
  Register user information  ${user_name}   ${password}   ${first_name}   ${last_name}   ${phone_number}

Login Credentials
  [Documentation]   This test case verifies that the user is able to login using correct credentials
  ...               User can review the correct information
  Login with credentials    ${user_name}   ${password}
  Review the user information   ${user_name}   ${first_name}    ${last_name}   ${phone_number}

*** Keywords ***
Suite setup
  Open Login Page  ${URL}  ${browser}

Suite Teardown
  Close Browser

Open Login Page
  [Documentation]   This keyword will open the web page
  [Arguments]    ${url}  ${browser}
  Open Browser   ${url}  ${browser}

Register user information
  [Documentation]   This keyword will register user information
  [Arguments]    ${user_name}  ${password}  ${first_name}   ${last_name}    ${phone_number}
  Page should contain   Register
  Click element         //a[@href="/register"]
  Click element         //*[@id="username"]
  Input text            //*[@id="username"]   ${user_name}
  Click element         //*[@id="password"]
  Input text            //*[@id="password"]   ${password}
  Click element         //*[@id="firstname"]
  Input text            //*[@id="firstname"]  ${first_name}
  Click element         //*[@id="lastname"]
  Input text            //*[@id="lastname"]   ${last_name}
  Click element         //*[@id="phone"]
  Input text            //*[@id="phone"]      ${phone_number}
  Click element         //*[@value="Register"]
  Page should contain   Log In

Login with credentials
  [Documentation]   This keyword will input credentials
  [Arguments]    ${user_name}  ${password}
  Click element   //a[@href="/login"]
  Click element   //*[@id="username"]
  Input text      //*[@id="username"]   ${user_name}
  Click element   //*[@id="password"]
  Input text      //*[@id="password"]   ${password}
  Click element   //*[@value="Log In"]

Review the user information
  [Documentation]   This keyword check the user information and logs out from the page
  [Arguments]    ${user_id}  ${user_name}  ${user_last_name}  ${user_phone_number}
  Page should contain    ${user_id}
  Page Should Contain    ${user_name}
  Page Should Contain    ${user_last_name}
  Page Should Contain    ${user_phone_number}
  Click element          //a[@href="/logout"]
  Page should contain    Log In